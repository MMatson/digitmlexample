import pickle
import gzip
import numpy as np


def load_data():
    f = gzip.open('mnist.pkl.gz', 'rb')
    training_data, validation_data, test_data = pickle.load(f, encoding="latin1")
    f.close()
    return (training_data, validation_data, test_data)


def load_data_wrapper():
    raw_train_data, raw_valid_data, raw_test_data = load_data()
    training_inputs = [np.reshape(x,(784, 1)) for x in raw_train_data[0]]
    training_results = [vectorized_result(y) for y in raw_train_data[1]]
    training_data = zip(training_inputs, training_results)
    validation_inputs = [np.reshape(x, (784, 1)) for x in raw_valid_data[0]]
    validation_data = zip(validation_inputs, raw_valid_data[1])
    test_inputs = [np.reshape(x, (784, 1)) for x in raw_test_data[0]]
    test_data = zip(test_inputs, raw_test_data[1])
    return (training_data, validation_data, test_data)


def vectorized_result(a):
    v = np.zeros((10, 1))
    v[a] = 1.0
    return v