import mnist_loader
import digit_ml
import digit_ml_net2


training_data, validation_data, test_data = mnist_loader.load_data_wrapper()
training_data = list(training_data)

# net = digit_ml.Network([784, 30, 10])
# net.SGD(training_data, 10, 10, 3.0, test_data=test_data)


# net = digit_ml_net2.Network([784, 30, 10], cost=digit_ml_net2.CrossEntropyCost)
# net.large_weight_initializer()
# net.SGD(training_data, 30, 10, 0.1, lmbda = 5.0,evaluation_data=validation_data,
#     monitor_evaluation_accuracy=True)

net = digit_ml_net2.Network([784, 30, 10], cost=digit_ml_net2.CrossEntropyCost)
net.large_weight_initializer()
net.SGD(training_data[:1000], 30, 10, 0.5,
    lmbda=5.0,
    evaluation_data=validation_data,
    monitor_evaluation_accuracy=True,
    monitor_training_cost=True,
    early_stopping_n=10)